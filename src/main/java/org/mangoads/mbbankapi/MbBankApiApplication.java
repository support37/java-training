package org.mangoads.mbbankapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MbBankApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MbBankApiApplication.class, args);
	}

}
