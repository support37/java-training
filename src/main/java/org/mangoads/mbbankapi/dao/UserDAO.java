package org.mangoads.mbbankapi.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mangoads.mbbankapi.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {

    private static final Map<String, User> empMap = new HashMap<String, User>();
 
    static {
        initEmps();
    }
 
    private static void initEmps() {
        User emp1 = new User("E01", "Smith", "Clerk");
        User emp2 = new User("E02", "Allen", "Salesman");
        User emp3 = new User("E03", "Jones", "Manager");
 
        empMap.put(emp1.getEmpNo(), emp1);
        empMap.put(emp2.getEmpNo(), emp2);
        empMap.put(emp3.getEmpNo(), emp3);
    }
 
    public User getUser(String empNo) {
        return empMap.get(empNo);
    }
 
    public User addUser(User emp) {
        empMap.put(emp.getEmpNo(), emp);
        return emp;
    }
 
    public User updateUser(User emp) {
        empMap.put(emp.getEmpNo(), emp);
        return emp;
    }
 
    public void deleteUser(String empNo) {
        empMap.remove(empNo);
    }
 
    public List<User> getAllUsers() {
        Collection<User> c = empMap.values();
        List<User> list = new ArrayList<User>();
        list.addAll(c);
        return list;
    }
 
}
