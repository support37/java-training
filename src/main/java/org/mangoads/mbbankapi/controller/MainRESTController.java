package org.mangoads.mbbankapi.controller;

import java.util.List;

import org.mangoads.mbbankapi.dao.UserDAO;
import org.mangoads.mbbankapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@RestController

public class MainRESTController {
 
    @Autowired
    private UserDAO UserDAO;
 
    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to RestTemplate Example.";
    }
 
    // URL:
    // http://localhost:8080/SomeContextPath/Users
    // http://localhost:8080/SomeContextPath/Users.xml
    // http://localhost:8080/SomeContextPath/Users.json
    @RequestMapping(value = "/Users", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public List<User> getUsers() {
        List<User> list = UserDAO.getAllUsers();
        return list;
    }
 
    // URL:
    // http://localhost:8080/SomeContextPath/User/{empNo}
    // http://localhost:8080/SomeContextPath/User/{empNo}.xml
    // http://localhost:8080/SomeContextPath/User/{empNo}.json
    @RequestMapping(value = "/User/{empNo}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User getUser(@PathVariable("empNo") String empNo) {
        return UserDAO.getUser(empNo);
    }
 
    // URL:
    // http://localhost:8080/SomeContextPath/User
    // http://localhost:8080/SomeContextPath/User.xml
    // http://localhost:8080/SomeContextPath/User.json
 
    @RequestMapping(value = "/User", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User addUser(@RequestBody User emp) {
 
        System.out.println("(Service Side) Creating User: " + emp.getEmpNo());
 
        return UserDAO.addUser(emp);
    }
 
    // URL:
    // http://localhost:8080/SomeContextPath/User
    // http://localhost:8080/SomeContextPath/User.xml
    // http://localhost:8080/SomeContextPath/User.json
    @RequestMapping(value = "/User", //
            method = RequestMethod.PUT, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User updateUser(@RequestBody User emp) {
 
        System.out.println("(Service Side) Editing User: " + emp.getEmpNo());
 
        return UserDAO.updateUser(emp);
    }
 
    // URL:
    // http://localhost:8080/SomeContextPath/User/{empNo}
    @RequestMapping(value = "/User/{empNo}", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public void deleteUser(@PathVariable("empNo") String empNo) {
 
        System.out.println("(Service Side) Deleting User: " + empNo);
 
        UserDAO.deleteUser(empNo);
    }
 
}